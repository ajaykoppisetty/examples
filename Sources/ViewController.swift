//
//  ViewController.swift
//  CouchbaseLiteSwiftExample
//
//  Created by Marco Betschart on 19.04.16.
//  Copyright © 2016 MANDELKIND. All rights reserved.
//

import UIKit
import CouchbaseLiteSwift


class ViewController: UIViewController {

	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let id = UUID().uuidString.components(separatedBy: "-").first{
			do{
				try Person.create([
					"givenName": "Donald" as AnyObject?,
					"familyName": "Duck \(id)" as AnyObject?,
					"age": arc4random_uniform(100) + 1 as AnyObject?,
					"birthday": Date() as AnyObject?
					]).save()
				
			} catch let error as NSError{
				print(error)
			}
		}
		
		let allPeople = CLSQuery<Person>()
		let adultPeople = CLSQuery<Person>().conditions("age > 18")
		
		print("\(adultPeople.count) of \(allPeople.count) people are older than 18:")
		for person in adultPeople{
			print(person.givenName!,person.familyName!)
		}
	}
}
